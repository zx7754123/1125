package com.example.sharepreferences

import android.app.Dialog
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import android.widget.Toast
import androidx.appcompat.app.AlertDialog

class MainActivity : AppCompatActivity() {

    private lateinit var edAcc: EditText
    private lateinit var edPassword: EditText
    private lateinit var ibOk: ImageButton
    private lateinit var btreg: Button
    private var userAcc = ""
    private var userPassword = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        edAcc = findViewById(R.id.etAcc)
        edPassword = findViewById(R.id.etPassword)
        ibOk = findViewById(R.id.ibOk)

        ibOk.setOnClickListener {view ->
            val acc = edAcc.text.toString()
            val password = edPassword.text.toString()
//            Toast.makeText(this, "Account: $acc\nPassword: $password", Toast.LENGTH_LONG).show()

            if (acc == userAcc && password == userPassword){
                val altDlgBld = AlertDialog.Builder(this)
                altDlgBld.setMessage("登入成功")
                altDlgBld.setCancelable(false)
                altDlgBld.setPositiveButton("OK") {dialog, which -> }
                altDlgBld.show()
            } else {
                val altDlgBld = AlertDialog.Builder(this)
                altDlgBld.setMessage("帳號或是密碼錯誤,是否提示?")
                altDlgBld.setCancelable(false)
                altDlgBld.setPositiveButton("YES") { dialog, which ->
                    Toast.makeText(this, "aaa/bbb", Toast.LENGTH_LONG).show()
                }
                altDlgBld.setNegativeButton("NO") { dialog, which -> }
                altDlgBld.show()
            }
            val sp = getSharedPreferences("app_data", Context.MODE_PRIVATE)
            sp.edit()
                .putString("acc", acc)
                .putString("password", password)
                .commit()
        }

        val sp = getSharedPreferences("app_data", Context.MODE_PRIVATE)
        val acc = sp.getString("acc", "")
        val password = sp.getString("password", "")
        acc?.let { if (acc.isNotEmpty()) edAcc.setText(acc) }
        password?.let { if (password.isNotEmpty()) edPassword.setText(password) }

        btreg = findViewById(R.id.btreg)
        btreg.setOnClickListener(){view ->
            val dlg = Dialog(this)
            dlg.setContentView(R.layout.dlg_reg)
            dlg.setCancelable(false)

            val  etAcc = dlg.findViewById<EditText>(R.id.etAcc)
            val etPassword = dlg.findViewById<EditText>(R.id.etPassword)
            val ibOK = dlg.findViewById<ImageButton>(R.id.ibOk)

            ibOK.setOnClickListener(){view ->
                userAcc = etAcc.text.toString()
                userPassword = etPassword.text.toString()
                dlg.dismiss()
            }
            dlg.show()
        }
    }
}